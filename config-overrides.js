module.exports = {
  webpack: function(config, env) {
    config.module.rules.push({
      test: /\.scss$/,
      use: [
        {
          loader: 'sass-resources-loader',
          options: {
            resources: ['./src/assets/styles/index.scss'],
          },
        },
      ],
    });

    return config;
  }
}
