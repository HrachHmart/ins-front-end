import React from "react";
import "./contract.tableColumn.scss";

export default function ContractTableColumn({ onShowCreate }) {
  return (
    <tr className={"contract_table_column"}>
      <th>Contract ID</th>
      <th>Premium</th>
      <th>Start Date</th>
      <th>Termination Date</th>
      <th>
        <span onClick={onShowCreate}>+</span>
      </th>
    </tr>
  );
}
