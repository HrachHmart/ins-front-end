import React, { useCallback, useState } from "react";
import Calendar from "react-calendar";

import "react-calendar/dist/Calendar.css";

import "./contract.tableRow.scss";
import MainButton from "../../buttons/main.button";
import { terminateContract_req } from "../../../api/admin.api";

export default function ContractTableRow({ contract = {}, onTerminate }) {
  const [open, setOpen] = useState(false);
  const [showCalendar, setShowCalendar] = useState(false);
  const [terminationDate, setTerminationDate] = useState(
    contract.terminationDate || new Date()
  );
  const [errMessage, setErrMessage] = useState("");

  const openClose = useCallback(() => {
    setOpen(!open);
  }, [open]);

  const showHideCalendar = useCallback(() => {
    if (!contract.terminationDate) {
      setShowCalendar(!showCalendar);
      setErrMessage("");
    }
  }, [contract, showCalendar]);

  const onTerminationDateChange = useCallback((date) => {
    setShowCalendar(false);
    setTerminationDate(date);
  }, []);

  const terminateContract = useCallback(async () => {
    try {
      setErrMessage("");

      await terminateContract_req({
        contractId: contract.contractId,
        terminationDate,
      });

      if (typeof onTerminate === "function") {
        onTerminate(terminationDate);
      }
    } catch (e) {
      console.log("ContractTableRow -> terminateContract() Error:", e);
      if (e?.response?.data?.message) {
        setErrMessage(e.response.data.message);
      }
    }
  }, [contract]);

  return (
    <>
      <tr className="contract_table_row" onClick={openClose}>
        <td>{contract.contractId}</td>
        <td>{contract.premium}</td>
        <td>{contract.startDate}</td>
        <td>{contract.terminationDate || "-"}</td>
        <td>
          <span className={`arrow${open ? " open" : ""}`}>▼</span>
        </td>
      </tr>
      {open && (
        <tr className="contract_table_row collapse">
          <td></td>
          <td></td>
          <td></td>
          <td>
            <div className="input_wrapper" onClick={showHideCalendar}>
              <input
                type="text"
                placeholder={"Termination Date"}
                disabled
                value={contract.terminatedDate || terminationDate}
              />
            </div>
            {errMessage && (
              <>
                <span className={"err_message"}>{errMessage}</span>
                <br />
              </>
            )}

            <MainButton
              disabled={!!contract.terminationDate}
              onClick={terminateContract}
            >
              Terminate
            </MainButton>
            {showCalendar && (
              <div className="calendar_wrapper">
                <Calendar
                  onChange={onTerminationDateChange}
                  value={terminationDate}
                />
              </div>
            )}
          </td>
          <td />
        </tr>
      )}
    </>
  );
}
