import React, { useCallback, useState } from "react";
import Calendar from "react-calendar";

import "react-calendar/dist/Calendar.css";

import "./contractCreate.tableRow.scss";

import MainInput from "../../inputs/main/main.input";
import { createContract_req } from "../../../api/admin.api";

export default function ContractCreateTableRow({ onCreate }) {
  const [contract, setContract] = useState({
    startDate: new Date(),
  });
  const [isValid, setIsValid] = useState({
    contractId: true,
    premium: true,
    startDate: true,
  });

  const [showStartCalendar, setShowStartCalendar] = useState(false);
  const [createErr, setCreateErr] = useState("");

  const showHideStartCalendar = useCallback(() => {
    setShowStartCalendar(!showStartCalendar);
  }, [showStartCalendar]);

  const onStartDateChange = useCallback((date) => {
    setShowStartCalendar(false);
    onContractChange("startDate", new Date(date));
  }, []);

  const onContractChange = useCallback(
    (field, value) => {
      const _contract = { ...contract };
      _contract[field] = value;
      setContract(_contract);
    },
    [contract]
  );

  const validate = useCallback(() => {
    let allValid = true;
    const _isValid = {
      contractId: true,
      premium: true,
      startDate: true,
    };

    if (!contract.contractId) {
      _isValid.contractId = false;
      allValid = false;
    }

    if (!contract.premium || contract.premium < 0) {
      _isValid.premium = false;
      allValid = false;
    }

    setIsValid(_isValid);

    if (!allValid) {
      return;
    }

    createContract(contract);
  }, [contract, isValid]);

  const createContract = useCallback(async (contract) => {
    try {
      setCreateErr("");
      await createContract_req(contract);
      if (typeof onCreate === "function") {
        onCreate(contract);
      }
    } catch (e) {
      console.log("ContractCreateTableRow -> createContract Error:", e);
      if (e?.response?.data?.message) {
        setCreateErr(e.response.data.message);
      }
    }
  }, []);

  return (
    <>
      <tr className="contract_table_create_row">
        <td>
          <MainInput
            type="text"
            placeholder={"Contract ID"}
            onChange={(e) => onContractChange("contractId", e.target.value)}
          />
          {!isValid.contractId && (
            <span className={"err_message"}>Contract ID is not valid</span>
          )}
          {createErr && <span className={"err_message"}>{createErr}</span>}
        </td>
        <td>
          <MainInput
            type="number"
            placeholder={"Premium"}
            onChange={(e) =>
              onContractChange("premium", Number(e.target.value))
            }
          />
          {!isValid.premium && (
            <span className={"err_message"}>Premium is not valid</span>
          )}
        </td>
        <td>
          <div className="input_wrapper" onClick={showHideStartCalendar}>
            <MainInput
              type="text"
              placeholder={"Start Date"}
              value={contract.startDate}
              onChange={() => {}}
            />
            {!isValid.startDate && (
              <span className={"err_message"}>Start Date is not valid</span>
            )}
          </div>
          {showStartCalendar && (
            <div className="calendar_wrapper">
              <Calendar
                onChange={onStartDateChange}
                value={contract.startDate}
              />
            </div>
          )}
        </td>
        <td>-</td>
        <td>
          <span className={"save_icon"} onClick={validate}>
            ✅
          </span>
        </td>
      </tr>
    </>
  );
}
