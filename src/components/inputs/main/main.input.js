import "./main.input.scss";

export default function MainInput(props) {
  return <input className={"main_input"} {...props} />;
}
