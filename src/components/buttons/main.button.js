import "./main.button.scss";

export default function MainButton({ children, disabled, onClick }) {
  return (
    <button className={"main_button"} disabled={disabled} onClick={onClick}>
      {children}
    </button>
  );
}
