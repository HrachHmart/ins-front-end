import { useState, useEffect, useCallback } from "react";

import "./home.page.scss";

import ContractTableColumn from "../../components/tableColumns/contract/contract.tableColumn";
import ContractTableRow from "../../components/tableRows/contract/contract.tableRow";
import { getContracts_req } from "../../api/admin.api";
import MainButton from "../../components/buttons/main.button";
import ContractCreateTableRow from "../../components/tableRows/contractCreate/contractCreate.tableRow";

const limit = 10;

function HomePage() {
  const [contracts, setContracts] = useState(null);
  const [page, setPage] = useState(1);

  const [showCreate, setShowCreate] = useState(false);

  const getContracts = useCallback(async () => {
    try {
      const response = await getContracts_req({ page, limit });

      if (response?.data) {
        if (contracts && contracts.length) {
          setContracts(contracts.concat(response.data));
        } else {
          setContracts(response.data);
        }
      }
    } catch (e) {
      console.log("HomePage -> getContracts() Error:", e);
    }
  }, [page]);

  useEffect(() => {
    getContracts();
  }, [page]);

  const loadMore = useCallback(() => {
    if (contracts && contracts.length % limit === 0) {
      setPage(page + 1);
    }
  }, [contracts, page]);

  const onTerminate = useCallback(
    (contract, index, terminationDate) => {
      const _contracts = [...contracts];
      try {
        if (terminationDate instanceof Date) {
          _contracts[index].terminationDate = terminationDate.toISOString();
        }
        setContracts(_contracts);
      } catch (e) {
        console.log("HomePage -> onTerminate Wrong date Error:", e);
      }
    },
    [contracts]
  );

  const onShowCreate = useCallback(() => {
    setShowCreate(!showCreate);
  }, [showCreate]);

  const onCreate = useCallback(
    (contract) => {
      const _contracts = [...contracts];
      try {
        if (contract.startDate instanceof Date) {
          contract.startDate = contract.startDate.toISOString();
        }
        _contracts.unshift(contract);
        setContracts(_contracts);
        setShowCreate(false);
      } catch (e) {
        console.log("HomePage -> onCreate Wrong date Error:", e);
      }
    },
    [contracts]
  );

  return (
    <div id="home-page" className="page">
      <h1>Contracts</h1>
      <br />
      <table
        cellPadding="0"
        cellSpacing="0"
        border="0"
        className="contracts_table"
      >
        <thead>
          <ContractTableColumn onShowCreate={onShowCreate} />
        </thead>
        <tbody>
          {showCreate && <ContractCreateTableRow onCreate={onCreate} />}
          {contracts?.length ? (
            contracts.map((contract, index) => (
              <ContractTableRow
                key={contract.contractId}
                contract={contract}
                onTerminate={(terminationDate) =>
                  onTerminate(contract, index, terminationDate)
                }
              />
            ))
          ) : (
            <></>
          )}
        </tbody>
      </table>
      <br />
      <MainButton onClick={loadMore}>Load More</MainButton>
    </div>
  );
}

export default HomePage;
