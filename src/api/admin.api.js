import { axiosInstanceV1 } from "./axiosInstance";
import { parseResponse } from "./helper";

const BASE = "/admin";

export function getContracts_req({ page, limit }) {
  return parseResponse(
    axiosInstanceV1.get(BASE + "/contract/list", {
      params: {
        page,
        limit,
      },
    })
  );
}

export function createContract_req({ contractId, premium, startDate }) {
  return parseResponse(
    axiosInstanceV1.post(BASE + "/contract/create", {
      contractId,
      premium,
      startDate,
    })
  );
}

export function terminateContract_req({ contractId, terminationDate }) {
  return parseResponse(
    axiosInstanceV1.put(BASE + "/contract/terminate", {
      contractId,
      terminationDate,
    })
  );
}
